class_name Player extends Node

var peer_id: int
var character_data: CharacterData = null

func _sync():
	if Connection.is_authority(self) and character_data != null:
		var json := JSONConverter.json_from_resource(character_data)
		_update_character_data.rpc(json)

@rpc
func _update_character_data(json: Dictionary):
	if character_data == null:
		character_data = CharacterData.new()
	JSONConverter.update_resource_from_json(character_data, json)
