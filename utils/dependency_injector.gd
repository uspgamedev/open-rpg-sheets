extends Node

class Injection:
	var property_name: StringName
	var value
	func _init(the_property_name: StringName, the_value):
		property_name = the_property_name
		value = the_value

@onready
var _injections := {}

func inject(group_name: StringName, property_name: StringName, value):
	_injections[group_name] = Injection.new(property_name, value)

func _process(_delta):
	for group_name in _injections:
		var injection := _injections[group_name] as Injection
		for node in get_tree().get_nodes_in_group(group_name):
			node.set(injection.property_name, injection.value)
