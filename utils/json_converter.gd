class_name JSONConverter extends Object

const SERIALIZABLE_PROPERTY = \
		PROPERTY_USAGE_STORAGE | \
		PROPERTY_USAGE_EDITOR | \
		PROPERTY_USAGE_SCRIPT_VARIABLE

func _init():
	@warning_ignore(assert_always_false)
	assert(false, 'This class should only be used statically')

static func json_from_resource(resource: Resource) -> Dictionary:
	var json = {}
	for property in resource.get_property_list():
		if property.usage == SERIALIZABLE_PROPERTY:
			json[property.name] = resource.get(property.name)
	return json

static func update_resource_from_json(
		resource: Resource,
		json: Dictionary,
) -> void:
	for property in resource.get_property_list():
		if property.usage == SERIALIZABLE_PROPERTY:
			resource.set(property.name, json[property.name])
