class_name PlayerSheets extends MarginContainer

@export
var sheet_scn: PackedScene

func _on_player_connected(player: Player):
	var sheet := sheet_scn.instantiate() as Sheet
	sheet.player = player
	%SheetList.add_child(sheet)

func _on_player_disconnected(player: Player):
	for sheet in %SheetList.get_children():
		if sheet is Sheet and sheet.player == player:
			%SheetList.remove_child(sheet)
			break

func _on_connection_state_changed(new_state: Connection.State):
	if new_state == Connection.State.IDLE:
		while %SheetList.get_child_count() > 0:
			%SheetList.remove_child(%SheetList.get_child(0))
