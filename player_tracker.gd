class_name PlayerTracker extends Node

@export
var player_scn: PackedScene

func add_player(peer_id: int, player_name: String) -> Player:
	var player = player_scn.instantiate()
	player.set_multiplayer_authority(peer_id)
	player.peer_id = peer_id
	player.name = player_name
	if multiplayer.get_unique_id() == peer_id:
		player.character_data = load('res://characters/sample.tres')
	add_child(player)
	return player

func get_player(peer_id: int) -> Player:
	for player in get_children():
		if player is Player and player.peer_id == peer_id:
			return player
	return null

func remove_player(peer_id: int) -> Player:
	var player := get_player(peer_id)
	remove_child(player)
	return player

func remove_all_players():
	while get_child_count() > 0:
		remove_child(get_child(0))
