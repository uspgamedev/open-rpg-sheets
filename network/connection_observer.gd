class_name ConnectionObserver extends Node

signal state_changed(new_state: Connection.State)
signal player_connected(player: Player)
signal player_disconnected(player: Player)

func _ready():
	add_to_group(Connection.OBSERVER_GROUP)
