class_name ConnectionController extends Node

var connection: Connection = null

func _ready():
	add_to_group(Connection.CONTROLLER_GROUP)

func join(ip: String, port: int):
	assert(connection != null)
	connection.join(ip, port)

func leave():
	assert(connection != null)
	connection.leave()
