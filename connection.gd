class_name Connection extends Node

const OBSERVER_GROUP := &'CONNECTION_OBSERVER_GROUP'
const CONTROLLER_GROUP := &'CONNECTION_CONTROLLER_GROUP'

enum State {
	IDLE,
	HOSTING,
	CONNECTING,
	JOINED,
}

@export
var default_port := 1337

@onready
var state := State.IDLE

@onready
var player_tracker := $PlayerTracker as PlayerTracker

var host_player: Player

func _ready():
	DependencyInjector.inject(CONTROLLER_GROUP, 'connection', self)
	multiplayer.connected_to_server.connect(_on_joined)
	multiplayer.connection_failed.connect(_on_join_failed)
	multiplayer.server_disconnected.connect(_on_disconnected)
	multiplayer.peer_connected.connect(_on_peer_connected)
	multiplayer.peer_disconnected.connect(_on_peer_disconnected)

func _process(_delta: float):
	if state == State.IDLE:
		_host()

func _change_state_to(new_state: State):
	state = new_state
	get_tree().call_group(OBSERVER_GROUP, 'emit_signal', 'state_changed', state)

func _host():
	var peer = ENetMultiplayerPeer.new()
	match peer.create_server(default_port):
		OK:
			multiplayer.multiplayer_peer = peer
			_change_state_to(State.HOSTING)
			host_player = _register_player(peer.get_unique_id(), 'Host')
			Log.info('Hosting at %s:%d' % ['localhost', default_port])
		ERR_ALREADY_IN_USE:
			Log.info('Invalid peer object')
		ERR_CANT_CREATE:
			Log.info('Could not create server!')

func join(ip: String, port: int):
	assert(state != State.JOINED and state != State.CONNECTING)
	if state == State.HOSTING:
		_close_connection()
	var peer = ENetMultiplayerPeer.new()
	match peer.create_client(ip, port):
		OK:
			multiplayer.multiplayer_peer = peer
			_change_state_to(State.CONNECTING)
			Log.info('Connecting to %s:%d...' % [ip, port])
		ERR_ALREADY_IN_USE:
			Log.info('Invalid peer object')
		ERR_CANT_CREATE:
			Log.info('Could not create connection!')

func leave():
	_close_connection()

func _on_joined():
	_change_state_to(State.JOINED)
	Log.info('Connected!')

func _on_join_failed():
	Log.info("Coult not connect to host")
	_close_connection()

func _on_disconnected():
	Log.info('Disconnected from host')
	_close_connection()

func _on_peer_connected(id: int):
	Log.info('Peer %d connected' % id)
	if is_multiplayer_authority():
		_register_player.rpc_id(id, host_player.peer_id, host_player.name)
		_register_player.rpc(id, "Player %d" % id)

func _on_peer_disconnected(id: int):
	Log.info('Peer %d disconencted' % id)
	if is_multiplayer_authority():
		_unregister_player.rpc(id)

func _close_connection():
	multiplayer.multiplayer_peer.close()
	multiplayer.multiplayer_peer = null
	player_tracker.remove_all_players()
	host_player = null
	_change_state_to(State.IDLE)

@rpc(call_local)
func _register_player(peer_id: int, player_name: String):
	var player := player_tracker.add_player(peer_id, player_name)
	get_tree().call_group(
			OBSERVER_GROUP,
			'emit_signal',
			'player_connected',
			player,
	)
	return player

@rpc(call_local)
func _unregister_player(peer_id):
	var player := player_tracker.remove_player(peer_id)
	get_tree().call_group(
			OBSERVER_GROUP,
			'emit_signal',
			'player_disconnected',
			player,
	)
	return player

static func is_authority(node: Node) -> bool:
	return node.get_multiplayer_authority() == node.multiplayer.get_unique_id()
