extends MarginContainer

signal join_requested(ip, port)
signal disconnection_requested()

func _on_join_requested():
	var ip: String = %IPEdit.text
	var port: int = %PortEdit.text.to_int()
	join_requested.emit(ip, port)

func _on_disconnection_requested():
	disconnection_requested.emit()

func _on_connection_state_changed(state: Connection.State):
	if state in [Connection.State.IDLE, Connection.State.HOSTING]:
		$JoinForm.show()
	else:
		$JoinForm.hide()
	if state in [Connection.State.JOINED]:
		$DisconnectForm.show()
	else:
		$DisconnectForm.hide()
