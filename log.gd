extends Node

signal message_logged()

enum Level {
	INFO,
	WARN,
	ERR,
}

const LEVEL_NAMES := {
	Level.INFO: 'INFO',
}

class Message:
	var level
	var text
	func _init(the_level: Level, the_text: String):
		level = the_level
		text = the_text

@onready
var messages: Array[Message] = []

func info(text: String):
	messages.append(Message.new(Level.INFO, text))
	message_logged.emit()
