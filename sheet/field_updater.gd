class_name FieldUpdater extends Node

@export var stat_name := ''
@export var target_property := ''
@export var format := '%d'

func _update_field(character_data: CharacterData) -> void:
	if owner != null:
		var stat = character_data.get(stat_name)
		owner.set(target_property, format % stat)

static func propagate(node: Node, character_data: CharacterData):
	node.propagate_call(&'_update_field', [character_data])
