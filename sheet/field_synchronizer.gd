class_name FieldSynchronizer extends FieldUpdater

enum SynchType {
	TEXT,
	NUMERIC,
}

@export var synch_type := SynchType.TEXT

func _sync_field(character_data: CharacterData) -> void:
	if owner != null:
		var field_value = converted_field_value()
		character_data.set(stat_name, field_value)
		owner.editable = true

func converted_field_value():
	var field_value = owner.get(target_property)
	match synch_type:
		SynchType.TEXT:
			return field_value
		SynchType.NUMERIC:
			return field_value.to_int()

static func propagate(node: Node, character_data: CharacterData):
	node.propagate_call(&'_sync_field', [character_data])
