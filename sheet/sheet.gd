class_name Sheet extends PanelContainer

var player: Player

@onready
var fields_filled := false

func _process(_delta):
	if player:
		%PlayerName.text = player.name
		if player.character_data:
			%Fields.show()
			if not _owns_sheet() or not fields_filled:
				FieldUpdater.propagate(%Fields, player.character_data)
				fields_filled = true
			else:
				FieldSynchronizer.propagate(%Fields, player.character_data)
		else:
			%Fields.hide()

func _owns_sheet() -> bool:
	return Connection.is_authority(player)
