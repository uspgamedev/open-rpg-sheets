class_name CharacterData extends Resource

@export
var name: String = 'Unnamed'

@export
var hit_points_max: int = 10

@export
var hit_points_current: int = 10
