extends Control

func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		propagate_call('release_focus')
