# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added 

- Unfocus control inputs when clicking "nowhere"
- Add nodes that automate sheet fields synchronization with character data
- Synchronize character sheets based on who owns the sheet
- Add autoload that injects dependencies in node groups
- Add helper that automates custom resource conversion from and to JSON
- Hide character stats if there is no data
- Add character data resource
- Show connected players' character sheets
- Add sheet and sheet listing nodes that describe the players' characters
- Add connection signals to announce players connecting and disconnecting
- Add player tracker node and track connected players
- Add player node
- Add a disconnect button to network menu that shows up after joining
- Add join feature to network menu
- Add join method to connection node
- Add a network menu to UI
- Add connection node that automatically starts hosting when app opens
- Add a GPLv3 license
- Add a simple logging system
- Add basic project files for Godot 4
- Add this CHANGELOG
