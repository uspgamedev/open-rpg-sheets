extends MarginContainer

func _ready():
	Log.message_logged.connect(self._log_updated)

func _log_updated():
	var contents := ""
	for message in Log.messages:
		contents += "[%4s] %s\n" % [Log.LEVEL_NAMES[message.level], message.text]
	%Contents.text = contents
